# Trashctl [![Build Status](https://travis-ci.org/vendion/trashctl.svg?branch=develop)](https://travis-ci.org/vendion/trashctl)

Trashctl is a command line tool to work with the FreeDesktop trash, and other
3rd party trash directories.

## Installing

Installing requires at least Go 1 to be installed, and trashctl can be installed
using:

    go get github.com/vendion/trashctl

## Usage

### Adding a file to the trash bin

Files can only be added to the main trash directory (following the FreeDesktop standard).
To add ad file or files use the following command

    trashctl add [filenames or path to files]

### Listing files in trash bins

Trashctl can list all files and various information about the file from not only the main
trash directory, but can work with 3rd party such as Dropbox, BTSync, Grive.

    trashctl list

### Removing files from trash bins

Files can be perminatly removed from the main trash directory by running

    trashctl rm [filenames]

### Empty trash bins

All files in the trash bins can be perminatly removed by running

    trashctl empty

## Inspiration

Trashctl is inspired by trash-cli tool, but everything is its own command, in
trashctl everything is a subcommand.  Trashctl also works with 3rd party trash
directories such as Grive, Insync, bt-sync.

## License

Trashctl is licensed under the 3 Clause BSD license.  For details on what this
means please see the LICENSE file provided with the source.
