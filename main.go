// Copyright 2014 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"os/user"
	"strings"
	"time"

	"github.com/divoxx/llog"
)

// VERSION contains the version number (semver.org)
const VERSION = "0.2"

var (
	l     *llog.Log
	level string
)

func usage() {
	command := os.Args[0]
	fmt.Fprintf(os.Stderr,
		`Usage: %s [options] {add|empty|list|rm} [file]
%s requires one of the following subcommands:
  add:      Add files to the default trash directory
  empty:    Delete files from the default trash directory
  list:     List contents of the trash directory
  restore:  Restores a file from the trash directroy
  rm:       Removes files in the trash directroy

Options:
  --debug:    Prints out debug messages
  --help:     Display this help message
  --verbose:  Prints out verbose messages
  --version:  Prints out the version numebr
`, command, command)
	// FIXME the default flags and values does not display
	//flag.PrintDefaults()
}

func main() {
	flag.Usage = usage
	debug := flag.Bool("debug", false, "Show debug output")
	version := flag.Bool("version", false, "Display the version number")
	verbose := flag.Bool("verbose", false, "Display verbose output")
	flag.Parse()

	if *version {
		fmt.Printf("%s version: %v\n", os.Args[0], VERSION)
		os.Exit(0)
	}

	if *debug {
		level = "DEBUG"
		l = llog.New(os.Stdout, llog.DEBUG)
	} else if *verbose {
		level = "INFO"
		l = llog.New(os.Stdout, llog.INFO)
	} else {
		l = llog.New(os.Stdout, llog.WARNING)
	}

	if flag.NArg() == 0 {
		logHandler("ERROR", "a subcommand is needed")
		usage()
		os.Exit(2)
	}

	logHandler("DEBUG", fmt.Sprintf("subcommand given: %s", flag.Arg(0)))
	switch flag.Arg(0) {
	case "add":
		args := flag.Args()
		args = getFilesList(args)
		logHandler("DEBUG", fmt.Sprintf("%#v", args))
		err := addFile(args)
		if err != nil {
			logHandler("ERROR", err.Error())
			os.Exit(1)
		}
	case "list":
		err := listFiles()
		if err != nil {
			logHandler("ERROR", err.Error())
			os.Exit(1)
		}
	case "restore":
		args := flag.Args()
		args = getFilesList(args)
		logHandler("DEBUG", fmt.Sprintf("%#v", args))
		err := restoreFiles(args)
		if err != nil {
			logHandler("ERROR", err.Error())
			os.Exit(1)
		}
	case "rm":
		args := flag.Args()
		args = getFilesList(args)
		logHandler("DEBUG", fmt.Sprintf("%#v", args))
		err := deleteFile(args)
		if err != nil {
			logHandler("ERROR", err.Error())
			os.Exit(1)
		}
	case "empty":
		err := emptyTrash()
		if err != nil {
			logHandler("ERROR", err.Error())
			os.Exit(1)
		}
	default:
		logHandler("ERROR", fmt.Sprintf("unknown subcommand: %s", flag.Arg(0)))
		usage()
		os.Exit(2)
	}
	os.Exit(0)
}

func logHandler(lvl, msg string) {
	// print message to the log if log level is high enough
	switch lvl {
	case "DEBUG":
		l.Debug("[DEBUG]", logTime(), msg)
	case "INFO":
		l.Info("[INFO]", logTime(), msg)
	case "WARN":
		l.Warning("[WARNING]", logTime(), msg)
	case "ERROR":
		l.Error("[ERROR]", logTime(), msg)
	default:
		return
	}
}

func logTime() string {
	return time.Now().Format(time.RFC3339)
}

// trashdirs returns a list of trash diretories to be handled.
func trashDirs() ([]string, error) {
	home, err := getHome()
	if err != nil {
		return nil, err
	}

	dirs := make([]string, 1)

	// get path to main trash directory
	dirs[0] = getTrashBase(home)
	if _, err = os.Stat(dirs[0]); err != nil && os.IsNotExist(err) {
		err = os.MkdirAll(dirs[0], 0744)
	}

	// get path to third party directories to handle
	trashDir := os.Getenv("TRASHCTL_PATH")
	if trashDir != "" {
		trashDirs := strings.Split(trashDir, ":")
		for _, dir := range trashDirs {
			dirs = append(dirs, dir)
		}
	}

	return dirs, nil
}

// getHome returns the path to the current users home directroy
func getHome() (string, error) {
	u, err := user.Current()
	if err != nil {
		logHandler("DEBUG", err.Error())
		return "", errors.New("could not get current user's home directory")
	}

	return u.HomeDir, nil
}

// getFilesList returns a slice similar to the one given but minus the very first element.
func getFilesList(args []string) []string {
	return args[1:]
}
