// +build darwin

package main

import (
	"os"
	"path"
)

func getTrashBase(home string) string {
	return path.Join(home, ".Trash")
}

func getTrashDir(trashBase string) (string, error) {
	return trashBase, nil
}

func getTrashInfoDir(trashBase string) (string, error) {
	home, err := getHome()
	if err != nil {
		return "", err
	}

	infoDir := path.Join(home, "Library", "trashctl")
	_, err = os.Stat(infoDir)
	if err != nil && os.IsNotExist(err) {
		err = os.MkdirAll(infoDir, 0744)
		if err != nil {
			return "", err
		}
	}

	return infoDir, err
}
