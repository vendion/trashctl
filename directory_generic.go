// +build !darwin,!windows

package main

import (
	"os"
	"path"
)

func getTrashBase(home string) string {
	xdgDataHome := os.Getenv("XDG_DATA_HOME")
	if xdgDataHome != "" {
		return path.Join(xdgDataHome, ".Trash")
	}

	return path.Join(home, ".local", "share", "Trash")
}

func getTrashDir(trashBase string) (string, error) {
	filesDir := path.Join(trashBase, "files")
	_, err := os.Stat(filesDir)
	if err != nil && os.IsNotExist(err) {
		err = os.MkdirAll(filesDir, 0744)
		if err != nil {
			return "", err
		}
	}

	return filesDir, err
}

func getTrashInfoDir(trashBase string) (string, error) {
	infoDir := path.Join(trashBase, "info")
	_, err := os.Stat(infoDir)
	if err != nil && os.IsNotExist(err) {
		err = os.MkdirAll(infoDir, 0744)
		if err != nil {
			return "", err
		}
	}

	return infoDir, err
}
