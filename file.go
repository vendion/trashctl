// Copyright 2014 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"

	"github.com/robfig/config"
)

// readTrashInfo reads in a info file and returns the original path of the file and date it was deleted.
func readTrashInfo(info string) (origPath, date string, err error) {
	c, err := config.Read(info, config.DEFAULT_COMMENT, config.DEFAULT_SEPARATOR, false, false)
	if err != nil {
		return
	}

	if !c.HasSection("Trash Info") {
		err = errors.New("not a valid trash info file")
		return
	}

	origPath, err = c.String("Trash Info", "Path")
	if err != nil {
		return
	}

	date, err = c.String("Trash Info", "DeletionDate")
	if err != nil {
		return
	}

	return
}

// createTrashInfo writes out the information file containging the orginal path of the file and current datatime.
func createTrashInfo(info, path string) error {
	c := config.New(config.DEFAULT_COMMENT, config.DEFAULT_SEPARATOR, false, false)

	if !c.AddSection("Trash Info") {
		return errors.New("could not create Trash Info section")
	}

	c.AddOption("Trash Info", "Path", path)
	c.AddOption("Trash Info", "DeletionDate", logTime())
	logHandler("DEBUG", fmt.Sprintf("Trash info file: %s", info))
	if err := c.WriteFile(info, 0644, ""); err != nil {
		return err
	}

	return nil
}

// emptyTrash removes all files from the trash directory.
func emptyTrash() error {
	trashDir, trashDirInfo, err := getDirs()
	if err != nil {
		return err
	}

	logHandler("DEBUG", fmt.Sprintf("trashDir: %s", trashDir))
	err = os.RemoveAll(trashDir)
	if err != nil {
		return err
	}

	logHandler("DEBUG", fmt.Sprintf("trashDirInfo: %s", trashDirInfo))
	err = os.RemoveAll(trashDirInfo)
	if err != nil {
		return err
	}

	return nil
}

// listFiles reads trash directories and list files in it and info about them.
func listFiles() error {
	dirs, err := trashDirs()
	if err != nil {
		return fmt.Errorf("failed to list contents of trash directory: %v\n", err)
	}

	for _, dir := range dirs {
		if dir != "" {
			infoDir, err := getTrashInfoDir(dir)
			// check to see if files are stored in a subdirectory
			if _, err = os.Stat(dir + "/files"); err == nil || os.IsExist(err) {
				dir = path.Join(dir, "files")
			}

			files, err := ioutil.ReadDir(dir)
			if err != nil {
				return fmt.Errorf("failed to read %s: %v\n", dir, err)
			}

			for _, file := range files {
				// Don't list OSX .DS_Store file
				if file.Name() == ".DS_Store" {
					continue
				}

				infoFile := fmt.Sprintf("%s/%s.trashinfo", infoDir, file.Name())
				var origPath string
				if _, err = os.Stat(infoFile); err == nil || os.IsExist(err) {
					logHandler("DEBUG", fmt.Sprintf("trashinfo file: %s", infoFile))
					origPath, _, err = readTrashInfo(infoFile)
					if err != nil {
						return fmt.Errorf("failed to parse .trashinfo file: %s", err.Error())
					}
				}
				size := float64(file.Size())
				size = size / 1024 / 1024 // return size in MiB
				if origPath != "" {
					fmt.Printf("%s\t%s\t%.2f MiB\n", file.Name(), origPath, size)
				} else {
					fmt.Printf("%s\t%.2f MiB\n", file.Name(), size)
				}
			}
		}
	}

	return nil
}

// addFile puts a file or files into the trash.
func addFile(files []string) error {
	home, err := getHome()
	if err != nil {
		return err
	}

	for _, file := range files {
		if _, err := os.Stat(file); err != nil && os.IsNotExist(err) {
			return fmt.Errorf("no file or directory: %s", file)
		}

		trashBase := getTrashBase(home)
		trashDir, err := getTrashDir(trashBase)
		if err != nil {
			return err
		}
		trashInfoDir, err := getTrashInfoDir(trashBase)
		if err != nil {
			return err
		}

		fileName := filepath.Base(file)
		logHandler("DEBUG", fmt.Sprintf("fileName: %s", fileName))

		trashFile := path.Join(trashDir, fileName)
		trashInfo := path.Join(trashInfoDir, fmt.Sprintf("%s.trashinfo", fileName))
		logHandler("DEBUG", fmt.Sprintf("trashFile: %s", trashFile))
		logHandler("DEBUG", fmt.Sprintf("trashInfo: %s", trashInfo))

		if !filepath.IsAbs(file) {
			var err error
			file, err = filepath.Abs(file)
			if err != nil {
				return err
			}
		}
		logHandler("DEBUG", fmt.Sprintf("file: %s", file))

		logHandler("DEBUG", fmt.Sprintf("trashDir: %s", trashDir))
		_, err = os.Stat(trashDir)
		if err != nil {
			logHandler("DEBUG", fmt.Sprintf("creating trash dir: %s", trashDir))
			err = os.MkdirAll(trashDir, 0755)
			if err != nil {
				return err
			}
		}

		_, err = os.Stat(trashInfoDir)
		if err != nil {
			logHandler("DEBUG", fmt.Sprintf("creating trash info dir: %s", trashInfoDir))
			err = os.MkdirAll(trashInfoDir, 0755)
			if err != nil {
				return err
			}
		}

		err = createTrashInfo(trashInfo, file)
		if err != nil {
			return err
		}

		if err = os.Rename(file, trashFile); err != nil {
			return errors.New("could not move file or directory to trash")
		}
	}

	return nil
}

func deleteFile(files []string) error {
	trashDir, trashInfoDir, err := getDirs()
	if err != nil {
		return err
	}

	for _, file := range files {
		trashFile := path.Join(trashDir, file)
		logHandler("DEBUG", fmt.Sprintf("trashFile: %s", trashFile))
		trashInfo := path.Join(trashInfoDir, fmt.Sprintf("%s.trashinfo", file))
		logHandler("DEBUG", fmt.Sprintf("trashInfo: %s", trashInfo))

		// check if trashinfo file exists
		_, err := os.Stat(trashInfo)
		if err == nil {
			err = os.Remove(trashInfo)
			if err != nil {
				return err
			}
		}

		_, err = os.Stat(trashFile)
		if err != nil {
			return fmt.Errorf("file \"%s\" not found", trashFile)
		}

		err = os.Remove(trashFile)
		if err != nil {
			return err
		}
	}

	return nil
}

func restoreFiles(files []string) error {
	trashDir, trashInfoDir, err := getDirs()
	if err != nil {
		return err
	}

	for _, file := range files {
		trashFile := path.Join(trashDir, file)
		logHandler("DEBUG", fmt.Sprintf("trashFile: %s", trashFile))
		trashInfo := path.Join(trashInfoDir, fmt.Sprintf("%s.trashinfo", file))
		logHandler("DEBUG", fmt.Sprintf("trashInfo: %s", trashInfo))

		_, err := os.Stat(trashFile)
		if err != nil {
			return fmt.Errorf("file \"%s\" not found in trash", file)
		}

		_, err = os.Stat(trashInfo)
		if err != nil {
			return fmt.Errorf("original path for \"%s\" is unknown, can't restore the file", file)
		}

		org, _, err := readTrashInfo(trashInfo)
		if err != nil {
			return err
		}
		logHandler("DEBUG", fmt.Sprintf("Restoring %s to: %s", file, org))
		err = os.Rename(trashFile, org)
		if err != nil {
			return nil
		}

		err = os.Remove(trashInfo)
		if err != nil {
			return fmt.Errorf("could not remove the trash info file for \"%s\"", file)
		}
	}

	return nil
}

func getDirs() (trashDir, trashInfoDir string, err error) {
	home, err := getHome()
	if err != nil {
		return
	}

	trashBase := getTrashBase(home)
	trashDir, err = getTrashDir(trashBase)
	if err != nil {
		return
	}
	trashInfoDir, err = getTrashInfoDir(trashBase)
	if err != nil {
		return
	}

	return
}
