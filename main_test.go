package main

import "testing"

func TestHome(t *testing.T) {
	h, _ := getHome()
	if h == "" {
		t.Log("did not get $HOME value")
		t.Fail()
	}
}

func TestXDGTrash(t *testing.T) {
	h, _ := getHome()
	s := getTrashBase(h)
	if s == "" {
		t.Log("did not get system trash directory")
		t.Fail()
	}
}

func TestGetTrashDirs(t *testing.T) {
	d, _ := trashDirs()
	if d == nil {
		t.Log("did not get back list of directories")
		t.Fail()
	}

	// check if we have a system directory
	if d[0] == "" {
		t.Log("did not get back a system directory")
	}
}
